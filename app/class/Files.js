// sert uniquement pour enregistrer l'image de l'utilisateur

const multer  = require('multer')
const fs = require('fs');
class Files {
    static saveFiles(){
        const storage = multer.diskStorage({
            destination: (req, file, callback) => {
                callback(null, './public/img/user/');
            },
            filename: (req, file, callback) => {
                callback(null,"logo.png");
            }
        })
        return multer({ storage: storage });
    }
}

module.exports = Files