// ici c'est tout qui est en rapport avec le user

const sqlite3 = require("sqlite3");
const { open } = require('sqlite');
class Users {
    // ici on verifie si un user existe
    static async userIsRegister(cb){
        const db = await open({
            filename: './bdd/database.db',
            driver: sqlite3.Database
        })
        const result = await db.all('SELECT * FROM Users WHERE ready = ?', 'true')
        if (result.length >= 1){
            cb({result: true})
        }else{
            cb({result: false})
        }
    }

    // ici on enregistre un user
    static async registerUser(pseudo, cb){
        const date = new Date();
        const db = await open({
            filename: './bdd/database.db',
            driver: sqlite3.Database
        })

        const exec_prepare = await db.prepare('INSERT INTO Users(ready, pseudo, datecreation) VALUES (?,?,?)')
        await exec_prepare.run('true', pseudo, date.toLocaleDateString())

        cb()
    }
}
module.exports = Users;