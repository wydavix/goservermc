// c'est une class pour tout se qui touche au server
const sqlite3 = require("sqlite3");
const { open } = require('sqlite');
const {download} = require("../Function/download")
const fs = require('fs');
const readline = require('readline');
const Client = require('./Client.js')
class Server extends Client{

    static sendCommandes(commande, cb){
        const client = this.client()
        client.connect().then(async () => {

            await client.run(commande);
            cb({type: 'message_success', content:'votre commandes a belle est bien était envoyer'})

            await client.close();

        }).catch((error) => {

            cb({type: 'message_error', content:'votre commandes n\'a pas pu être envoyer'})

        });
    }

    static async stopServer(cb){
        const client = this.client()
         await client.connect().then(async () => {

            await client.run('stop');
            cb({type: 'message_success', content:'votre serveur a belle est bien était fermer'})

            await client.close();

        }).catch(() => {

            cb({type: 'message_error', content:'votre serveur n\'a pas pu être fermer'})

        });
    }

    // ici c'est pour accepter les eula a partir du site (sa modifie dans la bdd et sa modifie le eula.txt du server selectionner
    static async accept_eula(path, id){
        const db = await open({
            filename: './bdd/database.db',
            driver: sqlite3.Database
        })

        // modification de la bdd par rapport a sont id

        const exec_prepare = await db.prepare('UPDATE Server SET eulais = ? WHERE id = ?')
        await exec_prepare.run("true", id)

        // modification du fichier eula.txt

        const file = `${path}/eula.txt`
        fs.readFile(file, {encoding: "utf-8"}, (err,result) =>{
            const content = "#By changing the setting below to TRUE you are indicating your agreement to our EULA (https://account.mojang.com/documents/minecraft_eula).\n"+
                            "#Tue Feb 09 14:43:23 CET 2021\n"+
                            "eula=true"
            fs.writeFile(file, content, {encoding: "utf-8"}, (err) =>{
                if (err) throw err;
            })
        })
    }

    static changeStaticServerPropertise(path){
        const file = `${path}/server.properties`
        const content = "#Minecraft server properties\n" +
            "#Tue Feb 09 14:00:27 CET 2021\n" +
            "spawn-protection=16\n" +
            "query.port=25565\n" +
            "generator-settings=\n" +
            "force-gamemode=false\n" +
            "allow-nether=true\n" +
            "gamemode=0\n" +
            "broadcast-console-to-ops=true\n" +
            "enable-query=true\n" +
            "player-idle-timeout=0\n" +
            "difficulty=1\n" +
            "spawn-monsters=true\n" +
            "broadcast-rcon-to-ops=true\n" +
            "op-permission-level=4\n" +
            "announce-player-achievements=true\n" +
            "pvp=true\n" +
            "snooper-enabled=true\n" +
            "level-type=DEFAULT\n" +
            "hardcore=false\n" +
            "enable-command-block=false\n" +
            "max-players=20\n" +
            "network-compression-threshold=256\n" +
            "resource-pack-sha1=\n" +
            "max-world-size=29999984\n" +
            "rcon.port=25575\n" +
            "server-port=25565\n" +
            "debug=false\n" +
            "server-ip=\n" +
            "spawn-npcs=true\n" +
            "password=abc123\n" +
            "allow-flight=false\n" +
            "level-name=world\n" +
            "view-distance=10\n" +
            "resource-pack=\n" +
            "spawn-animals=true\n" +
            "white-list=false\n" +
            "rcon.password=i>x22!5QV\n" +
            "generate-structures=true\n" +
            "max-build-height=256\n" +
            "online-mode=false\n" +
            "level-seed=\n" +
            "motd=Server Prepare By GoServerMc\n" +
            "enable-rcon=true\n"
        fs.writeFile(file, content, {encoding: "utf-8"}, (err) =>{
            if (err) throw err;
        })
    }

    static prepareStartServer(path, id, cb){
        this.accept_eula(path, id);
        this.changeStaticServerPropertise(path)
        cb()
    }

    // ici c'est pour start le serveur selectionner
    static start_server(path, minRam, maxRam){
        const child = require('child_process').spawn(
            'java', [`-Xms${minRam}M`,`-Xmx${maxRam}M`,`-jar`, 'server.jar'],
            {cwd: path}
        );
        child.stdout.on('data', (data) => {

        });
        child.stderr.on("data",  (data) => {

        });
    }

    //ici c'est pour enregistrer un nouveau serveur dans la bdd

    static async insertServerBdd(name, minRam, maxRam, version){
        const db = await open({
            filename: './bdd/database.db',
            driver: sqlite3.Database
        })

        const exec_prepare = await db.prepare('INSERT INTO Server(name, minRam, maxRam, version, path, eulais) VALUES (?,?,?,?,?,?)')
        await exec_prepare.run(name, minRam, maxRam, version, `./Servers/${name}/`, "false")
    }

    //ici c'est pour récuperer tout les serveurs dans la badd

    static async getAllServer(cb){
        const db = await open({
            filename: './bdd/database.db',
            driver: sqlite3.Database
        })

        const result = await db.all('SELECT * FROM Server')
        cb(result)
    }

    // ici c'est pour récuperer un serveur par rapport a son id
    static async getServerById(id, cb){
        const db = await open({
            filename: './bdd/database.db',
            driver: sqlite3.Database
        })
        const result = await db.all('SELECT * FROM Server WHERE id = ?', id)

        if (result.length >= 1){
            // la on renvoie les informations du serveur
            cb({
                    result: true,
                    id: result[0].id,
                    name: result[0].name,
                    version: result[0].version,
                    path: result[0].path,
                    minRam: result[0].minRam,
                    maxRam: result[0].maxRam,
                    eulais: result[0].eulais
                })
        }else {
            cb({result: false})
        }
    }

    // ici c'est pour créer un nouveau serveur

    static createServer(name, minRam, maxRam, version, cb){
        this.insertServerBdd(name, minRam, maxRam, version);
        fs.mkdir(`./Servers/${name}`, (e) => {
            if(!e || (e && e.code === "EEXIST")){
                download(version, `./Servers/${name}/server.jar`, (err) => {
                    if (err) {
                        console.error(err);
                        return;
                    }
                    const child = require('child_process').spawn(
                        'java', ['-Xms1024M','-Xmx1024M','-jar', 'server.jar'],
                        {cwd: `./Servers/${name}`}
                    );
                    child.stdout.on('data', (data) => {});
                    child.stderr.on("data",  (data) => {});

                    cb()
                })
            }
        });
    }

    // c'est pour afficher la console du serveur selectionner
    static console_viewer(path, io){
        const file = `${path}/logs/latest.log`
        fs.watchFile(file,{interval: 500}, (curr, prev) => {
            const rl = readline.createInterface({
                input: fs.createReadStream(file)
            });

            rl.on('line', (line) => {
                io.emit('console', line);
            });
        });
    }
}

module.exports = Server;