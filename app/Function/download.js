const fs = require('fs');
const request = require('request');
const sqlite3 = require("sqlite3");
const { open } = require('sqlite');

async function  download(version, dest, cb) {
    // on créé un stream d'écriture qui nous permettra
    // d'écrire au fur et à mesure que les données sont téléchargées
    const file = fs.createWriteStream(dest);

    let url;

    const db = await open({
        filename: './bdd/database.db',
        driver: sqlite3.Database
    })
    const result = await db.all('SELECT * FROM Versions WHERE version = ?', version)

    if (result.length >= 1){
        url = (result[0].url).toString()
        // on lance le téléchargement
        const sendReq = request.get(url);
        console.log(url)

        // on vérifie la validité du code de réponse HTTP
        sendReq.on('response', (response) => {
            if (response.statusCode !== 200) {
                return cb('Response status was ' + response.statusCode);
            }
        });

        // au cas où request rencontre une erreur
        // on efface le fichier partiellement écrit
        // puis on passe l'erreur au callback
        sendReq.on('error', (err) => {
            fs.unlink(dest);
            cb(err.message);
        });

        // écrit directement le fichier téléchargé
        sendReq.pipe(file);

        // lorsque le téléchargement est terminé
        // on appelle le callback
        file.on('finish', () => {
            // close étant asynchrone,
            // le cb est appelé lorsque close a terminé
            file.close(cb);
        });

        // si on rencontre une erreur lors de l'écriture du fichier
        // on efface le fichier puis on passe l'erreur au callback
        file.on('error', (err) => {
            // on efface le fichier sans attendre son effacement
            // on ne vérifie pas non plus les erreur pour l'effacement
            fs.unlink(dest);
            cb(err.message);
        });
    }else {
        cb({result: false})
    }
};

module.exports = {download}