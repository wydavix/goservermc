const config = require('./config.json')
const Users = require('./class/Users.js');
const Files  = require('./class/Files.js');
const Inputverif = require('./class/Inputverif.js')
const Server = require('./class/Server.js')
const fs = require('fs');
const readline = require('readline');
const express = require('express')
const bodyParser = require('body-parser')
const app = express()

// ajout de socket.io
const server = require('http').Server(app)
const io = require('socket.io')(server)

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))
app.use('/assets/', express.static('public'))

const views = `${__dirname}/views/`;
app.get('/', function (req, res) {
    Users.userIsRegister(cb => {
        if (cb.result === true){
            res.redirect('/panel')
        }else {
            res.redirect('/register')
        }
    })
})

app.get('/json', function (req, res) {
    res.status(200).json({"message":"ok"})
})

app.get('/panel', ((req, res) => {
    Users.userIsRegister(cb => {
        if (cb.result === true){
            res.sendFile('index.html', { root: `${views}/panel/` })
            setTimeout(() => {
                Server.getAllServer(cb => {
                    const result = cb;
                    for (let i = 0; i < result.length; i++){
                        io.emit('Server', result[i].name, result[i].version, result[i].id)
                    }
                })
            }, 1000)
        }else {
            res.redirect('/register')
        }
    })
}))

app.get('/servers/views/:id', (req, res) => {
    Server.getServerById(req.params.id, cb => {
        if (cb.result === true){
            res.sendFile('index.html', { root: `${views}/panel/servers/views/`})
            setTimeout(() => {
                io.emit('page_view_server', cb)
            }, 1000)
        }
    })
})

app.get('/register', ((req, res) => {
    Users.userIsRegister(cb => {
        if (cb.result === true){
            res.redirect('/panel')
        }else {
            fs.mkdir(`./public/img/user`, (e) => {
                if (!e || (e && e.code === "EEXIST")) {

                } else {
                    console.log(e);
                }
            })
            res.sendFile('index.html', { root: `${views}/register/` })
        }
    })
}))

app.get('*', ((req, res) => {
    res.status(404).send("404 not found");
}))

app.post('/register',Files.saveFiles().any(), (req, res)=> {
    Inputverif.isNotEmpty(req.body.pseudo, cb => {
        if (cb.result === true){
            Users.registerUser(req.body.pseudo, cb => {
                res.redirect('/')
            })
        }else {
            res.redirect('/register')
            setTimeout(() => {
                io.emit('error', 'Veuillez remplir tout les champs')
            }, 1000)
        }
    })
});



io.on('connection', (socket) => {
    socket.on('create_server', (name, minRam, maxRam, version) => {
        Server.createServer(name, minRam, maxRam, version, cb => {
            console.log("nice")
        })
    })

    socket.on('start_server', (path, minRam, maxRam) => {
        Server.start_server(path, minRam, maxRam)
        setTimeout(() => {
            const file = `${path}/logs/latest.log`
            fs.watchFile(file,{interval: 100}, (curr, prev) => {
                const rl = readline.createInterface({
                    input: fs.createReadStream(file)
                });

                rl.on('line', (line) => {
                    io.emit('console', line);
                });
            });
        })
    })

    socket.on('accept_eula', (path, id) => {
        Server.prepareStartServer(path, id, cb => {

        })
    })

    socket.on('enter_console', command => {
        Server.sendCommandes(command, cb => {
            if(cb){
                io.emit(cb.type, cb.content);
            }
        })
    });

    socket.on('stop_server', () => {
        Server.stopServer(cb => {

        })
    })
});

// on change app par server
server.listen(config.PORT, () => {
    console.log(`Votre GoServerMc est disponible sur localhost:${config.PORT} !`)
})