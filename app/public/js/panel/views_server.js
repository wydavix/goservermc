const socket = io();
const viewer_console = document.getElementById('console');
const title_page = document.getElementById('title_page');
const start_btn = document.getElementById('start');
const stop_btn = document.getElementById('stop');
const eula = document.getElementById('eula')

const form_enter_console = document.getElementById('enter_console');
const commandes_input = document.getElementById('commandes');

let id;
let path;
let minRam;
let maxRam;
let eulais;

form_enter_console.addEventListener('submit', e => {
    e.preventDefault();
    if (commandes_input.value !== undefined && commandes_input !== ''){
        socket.emit('enter_console', commandes_input.value)
        commandes_input.value = ""
    }
})

const accept_eula = document.getElementById('accept_eula');

accept_eula.addEventListener('click', (e) => {
    e.preventDefault();
    eula.classList.remove('views_eula')
    eula.classList.add('no_views_eula')
    setTimeout(() => {
        eula.style.display = 'none';
    }, 500)
    console.log("ok")
    socket.emit('accept_eula', path, id)
})

stop_btn.addEventListener('click', e => {
    e.preventDefault();
    socket.emit('stop_server')
})

start_btn.addEventListener('click', e => {
    e.preventDefault();
    socket.emit('start_server', path, minRam, maxRam)
})

socket.on('console', (line) => {
    const item = document.createElement('li');
    item.textContent = line;
    viewer_console.appendChild(item);
    item.classList.add('msg');
    viewer_console.scrollTo(0, viewer_console.scrollHeight);
});

socket.on('page_view_server', cb => {
    title_page.innerHTML = `Server : "${cb.name}"`
    id = cb.id;
    path = cb.path;
    minRam = cb.minRam;
    maxRam = cb.maxRam;
    eulais = cb.eulais;
    console.log(eulais)
    if (eulais === "false"){
        console.log(eulais)
        eula.style.display = 'flex';
        eula.classList.remove('no_views_eula')
        eula.classList.add('views_eula')
    }
})