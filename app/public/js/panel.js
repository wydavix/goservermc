const socket = io();
const div = document.querySelector('.views');
const div_all_server = document.querySelector('.div_all_server');

const creator_server = document.getElementById('creator-server');
const formcreator = document.getElementById('form-creator');
const name = document.getElementById('nom');
const minRam = document.getElementById('minram');
const maxRam = document.getElementById('maxnram');
const version = document.getElementById('version');

const btn_open_creator = document.getElementById('creator-btn')
const btn_close_creator = document.getElementById('creator-btn-close')

btn_open_creator.addEventListener('click', (e) => {
        e.preventDefault();
        creator_server.style.display = 'flex';
        creator_server.classList.remove('no_views_creator')
        creator_server.classList.add('views_creator')
})

btn_close_creator.addEventListener('click', (e) => {
        e.preventDefault();
        creator_server.classList.remove('views_creator')
        creator_server.classList.add('no_views_creator')
        setTimeout(() => {
                creator_server.style.display = 'none';
        }, 500)
})


let line = 0;
let div_line = document.createElement('div')
let div_content = document.createElement('div')
socket.on('Server', (name, version, id) => {
        if (line === 6){
            line = 0;
            div_line = document.createElement('div')
            div_content = document.createElement('div')
        }
        div_line.classList.add('line');
        div_content.classList.add('div_content');
        const server = document.createElement('a');
        const content_info = document.createElement('div');
        const name_server = document.createElement('div');
        const version_server = document.createElement('div');
        const content = document.createElement('div');
        const img_logo_mc = document.createElement('img');
        name_server.textContent = name;
        version_server.textContent = version;
        div_all_server.appendChild(div_line);
        div_line.appendChild(div_content);
        div_content.appendChild(server);
        server.href = `/servers/views/${id}`;
        server.appendChild(content);
        content.appendChild(img_logo_mc);
        img_logo_mc.src = '/assets/img/views/Minecraft_logo.png'
        img_logo_mc.classList.add('logo-mc');
        server.appendChild(content_info);
        content_info.appendChild(name_server);
        content_info.appendChild(version_server);
        content.classList.add('content');
        content_info.classList.add('content_info');
        server.classList.add('server');
        name_server.classList.add('name');
        version_server.classList.add('version');
        line++;
})

function verifInput(cb){
        if (
            name.value !== undefined && name.value !== "" && name.value !== null &&
            minRam.value !== undefined && minRam.value !== "" && minRam.value !== null &&
            maxRam.value !== undefined && maxRam.value !== "" && maxRam.value !== null &&
            version.value !== undefined && version.value !== "" && version.value !== null
        ){
                cb({result: true})
        }else{
                cb({result: false})
        }
}

formcreator.addEventListener('submit', e => {
        e.preventDefault();
        verifInput(cb => {
                if (cb.result === true){
                        socket.emit('create_server', name.value, minRam.value, maxRam.value, version.value)
                        creator_server.classList.remove('views_creator')
                        creator_server.classList.add('no_views_creator')
                        setTimeout(() => {
                                document.location.href="/panel";
                        }, 500)
                }
        })
})

socket.on('error', (msg) => {
    div.innerHTML = msg
});